﻿using System;
using System.Collections.Generic;

namespace ACM.BL
{
    public class OrderRepository
    {
        /// <summary>
        /// Retrieves one Order.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public Order Retrieve(int orderId)
        {
            Order order = new Order();

            //Temp
            if (orderId == 10)
            {
                order.OrderDate = new DateTimeOffset(2015, 2, 7, 0, 0, 0, new TimeSpan(-5, 0, 0));
            }

            return order;
        }

        public OrderDisplay RetrieveOrderDisplay(int orderId)
        {
            OrderDisplay orderDisplay = new OrderDisplay();

            //Code that retrieves the order fields

            //Temporary hard coded data
            if (orderId == 10)
            {
                orderDisplay.FirstName = "Bilbo";
                orderDisplay.LastName = "Baggins";
                orderDisplay.OrderDate = new DateTimeOffset(2014, 4, 14, 10, 00, 00, new TimeSpan(-5, 0, 0));
                orderDisplay.ShippingAddress = new Address()
                {
                    AddressType = 1,
                    StreetLine1 = "Bag End",
                    StreetLine2 = "Bagshot Row",
                    City = "Hobbiton",
                    State = "Shire",
                    Country = "Middle Earth",
                    PostalCode = "144"
                };

                orderDisplay.OrderDisplayItemList = new List<OrderDisplayItem>();
            }

            //Code that retrieves the order items
            
            //Temporary Hard-coded data
            if (orderId == 10)
            {
                var orderDisplayItem = new OrderDisplayItem()
                {
                    ProductName = "Sunflowers",
                    PurchasePrice = 15.96M,
                    OrderQuantity = 2
                };

                orderDisplay.OrderDisplayItemList.Add(orderDisplayItem);

                orderDisplayItem = new OrderDisplayItem()
                {
                    ProductName = "Rake",
                    PurchasePrice = 6M,
                    OrderQuantity = 1
                };

                orderDisplay.OrderDisplayItemList.Add(orderDisplayItem);
            }

             return orderDisplay;
        }

        /// <summary>
        /// Retrieves all orders.
        /// </summary>
        /// <returns></returns>
        public Order Retrieve()
        {
            return new Order();
        }

        public bool Save(Order order)
        {
            var success = true;

            if (order.HasChanges && order.IsValid)
            {
                if (order.IsNew)
                {
                    //Call an Insert Stored Proc.
                }
                else
                {
                    //Call an Update Stored Proc
                }
            }

            return success;
        }
    }
}
