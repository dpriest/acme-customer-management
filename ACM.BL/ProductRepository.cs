﻿using System;

namespace ACM.BL
{
    public class ProductRepository
    {
        /// <summary>
        /// Retrieves one product.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public Product Retrieve(int productId)
        {
            Product product = new Product(productId);

            Object myObj = new Object();

            Console.WriteLine("Object: " + myObj.ToString());
            Console.WriteLine("Product: " + product.ToString());

            // Temp
            if (productId == 2)
            {
                product.ProductName = "Sunflowers";
                product.ProductDescription = "Assorted Sized";
                product.CurrentPrice = 15.96m;
            }

            return product;
        }

        /// <summary>
        /// Retrieves all products.
        /// </summary>
        /// <returns></returns>
        public Product Retrieve()
        {
            return new Product();
        }

        public bool Save(Product product)
        {
            var success = true;

            if (product.HasChanges && product.IsValid)
            {
                if (product.IsNew)
                {
                    //Call an Insert Stored Proc.
                }
                else
                {
                    //Call an Update Stored Proc
                }
            }

            return success;
        }
    }
}
